/*
 * Copyright (c) 2006-2010 Sun Microsystems, Inc.
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to 
 * deal in the Software without restriction, including without limitation the 
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or 
 * sell copies of the Software, and to permit persons to whom the Software is 
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permissiIon notice shall be included in 
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE 
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 * DEALINGS IN THE SOFTWARE.
 **/
package org.sunspotworld;

import com.sun.spot.resources.transducers.ISwitchListener;
import com.sun.spot.resources.transducers.LEDColor;
import com.sun.spot.resources.transducers.SwitchEvent;
import com.sun.spot.sensorboard.peripheral.Servo;
import org.sunspotworld.lib.BlinkenLights;


import com.sun.spot.resources.Resources;
//import com.sun.spot.resources.transducers.IIOPin;
import com.sun.spot.resources.transducers.IAnalogInput;
//import com.sun.spot.sensorboard.io.AnalogInput;
import com.sun.spot.resources.transducers.ISwitch;
import com.sun.spot.resources.transducers.ITriColorLED;
import com.sun.spot.resources.transducers.ITriColorLEDArray;
import com.sun.spot.sensorboard.EDemoBoard;
import com.sun.spot.service.BootloaderListenerService;
import com.sun.spot.util.Utils;

import java.io.IOException;
import javax.microedition.midlet.MIDlet;
import javax.microedition.midlet.MIDletStateChangeException;
import java.lang.Math;

/**
 * This class is used to move a servo car consisting of two servos - one for
 * left wheel and the other for right wheel. To combine these servos properly,
 * this servo car moves forward/backward, turn right/left and rotate
 * clockwise/counterclockwise.
 *
 * The current implementation has 3 modes and you can change these "moving mode"
 * by pressing sw1. Mode 1 is "Normal" mode moving the car according to the tilt
 * of the remote controller. Mode 2 is "Reverse" mode moving the car in a
 * direction opposite to Mode 1. Mode 3 is "Rotation" mode only rotating the car
 * clockwise or counterclockwise according to the tilt.
 *
 * @author Tsuyoshi Miyake <Tsuyoshi.Miyake@Sun.COM>
 * @author Yuting Zhang<ytzhang@bu.edu>
 */
public class ServoSPOTonCar extends MIDlet implements ISwitchListener {

    private static final int SERVO_CENTER_VALUE = 1500;
    //private static final int SERVO_MAX_VALUE = 2000;
    //private static final int SERVO_MIN_VALUE = 1000;
    private static final int SERVO1_MAX_VALUE = 2000;
    private static final int SERVO1_MIN_VALUE = 1000;
    private static final int SERVO2_MAX_VALUE = 2000;
    private static final int SERVO2_MIN_VALUE = 1000;
    //private static final int SERVO_HIGH = 500;
    //private static final int SERVO_LOW = 300;
    private static final int SERVO1_HIGH = 20; //steering step high
    private static final int SERVO1_LOW = 10; //steering step low
    private static final int SERVO2_HIGH = 10; //speeding step high
    private static final int SERVO2_LOW = 5; //speeding step low
    private static final int PROG0 = 0; //default program
    private static final int PROG1 = 1; // reversed program
    //private static final int PROG2 = 2;
    
    /* The scanner for getting user input in calibrate speed and angle */
    // Devices
    private EDemoBoard eDemo = EDemoBoard.getInstance();
    private ISwitch sw1 = eDemo.getSwitches()[EDemoBoard.SW1];
    private ISwitch sw2 = eDemo.getSwitches()[EDemoBoard.SW2];
    private ITriColorLED[] leds = eDemo.getLEDs();
    private ITriColorLEDArray myLEDs = (ITriColorLEDArray) Resources.lookup(ITriColorLEDArray.class);
    // 1st servo for speed 
    private Servo speedServo = new Servo(eDemo.getOutputPins()[EDemoBoard.H1]);
    // 2nd servo for direction
    private Servo directionServo = new Servo(eDemo.getOutputPins()[EDemoBoard.H0]);
    private BlinkenLights progBlinker = new BlinkenLights(0, 3);
    private BlinkenLights velocityBlinker = new BlinkenLights(4, 7);
    private int current1 = SERVO_CENTER_VALUE;
    private int current2 = SERVO_CENTER_VALUE;
    private int step1 = SERVO1_LOW;
    private int step2 = SERVO2_LOW;
    private int program = PROG0;
    IAnalogInput frSensor = EDemoBoard.getInstance().getAnalogInputs()[EDemoBoard.A0];//frontright
    IAnalogInput flSensor = EDemoBoard.getInstance().getAnalogInputs()[EDemoBoard.A1];//frontleft
    IAnalogInput brSensor = EDemoBoard.getInstance().getAnalogInputs()[EDemoBoard.A2];//backright
    IAnalogInput blSensor = EDemoBoard.getInstance().getAnalogInputs()[EDemoBoard.A3];//backleft
    //private int servo1ForwardValue;
    //private int servo2ForwardValue;
    private int servo1Left = SERVO_CENTER_VALUE + SERVO1_LOW;
    private int servo1Right = SERVO_CENTER_VALUE - SERVO1_LOW;
    private int servo2Forward = SERVO_CENTER_VALUE + SERVO2_LOW;
    private int servo2Back = SERVO_CENTER_VALUE - SERVO2_LOW;
    private int speed = 1200;
    private boolean enableCalibration = false;
    private boolean calibrateSpeed = false;
    private boolean calibrateAngle = false;
    
    private int maxAngle = 5;
    private int wallDistance = 60;

    public ServoSPOTonCar() {
    }

    /**
     * BASIC STARTUP CODE *
     */
    protected void startApp() throws MIDletStateChangeException {
        System.out.println("Hello, world");
        BootloaderListenerService.getInstance().start();

        for (int i = 0; i < myLEDs.size(); i++) {
            myLEDs.getLED(i).setColor(LEDColor.GREEN);
            myLEDs.getLED(i).setOn();
        }
        Utils.sleep(500);
        for (int i = 0; i < myLEDs.size(); i++) {
            myLEDs.getLED(i).setOff();
        }
        
        
        // timeout 1000
//        TwoSidedArray robot = new TwoSidedArray(getAppProperty("buddyAddress"), Globals.READ_TIMEOUT);
//        try {
//            robot.startInput();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//        sw1.addISwitchListener(this);
//        sw2.addISwitchListener(this);
//
//        velocityBlinker.setColor(LEDColor.BLUE);
//        progBlinker.setColor(LEDColor.BLUE);
//
//        boolean error = false;

//        forward();
        for (int i = 0; i < myLEDs.size(); i++) {
            myLEDs.getLED(i).setColor(LEDColor.RED);
            myLEDs.getLED(i).setOn();
        }
        /* Do not go until the switch is pressed */
        if (enableCalibration)
        {
            System.out.println("Waiting for switch");
            while (true)
            {
                if (sw1.isClosed())
                {
                    System.out.println("Speed calibration");
                    calibrateSpeed = true;
                    break;
                }
                else if (sw2.isClosed())
                {
                    System.out.println("Angle calibration");
                    calibrateAngle = true;
                    break;
                }
            }
        }
        else
        {
            while (sw1.isOpen());
        }
        
        for (int ii = 0 ; ii < 2 ; ii++)
        {
            for (int i = 0; i < myLEDs.size(); i++) {
                myLEDs.getLED(i).setColor(LEDColor.GREEN);
                myLEDs.getLED(i).setOn();
            }
            Utils.sleep(500);
            for (int i = 0; i < myLEDs.size(); i++) {
                myLEDs.getLED(i).setOff();
            }
            Utils.sleep(500);
        }
        
        if (calibrateSpeed)
        {
            runSpeedCalibration();
        }
        
        if (calibrateAngle)
        {
            runAngleCalibration();
        }
        
//        progBlinker.startPsilon();
//        velocityBlinker.startPsilon();
        
        double carAngle = 0;
        DistPair distances;
       beginForwardMotion();
        int alternating = 0;
        while (true) {  
            if (alternating % 2 == 0)
            {
                speedServo.setValue(speed);
            }
            else if (alternating % 2 == 1)
            {
                speedServo.setValue(speed + 10);
            }
            
            carAngle = getCurrentAngle();
            distances = getCurrentDistance();
           // System.out.println(carAngle);
            adjustMovement(carAngle, distances);
            
            alternating++;
            speedServo.setValue(0);
            Utils.sleep(10);
//            
//            /* If angle or distance is sufficiently off, can force the car to stop to fix it */
//            
//            distanceOff = getCurrentDistance();
//            adjustDistance(10.0, distanceOff);


           // check();
//            Utils.sleep(20);
        }
    }
    
    /**
     * Runs the crawler at a slow speed for 1 seconds for measuring speed 
     */
    private void runSpeedCalibration()
    {
        myLEDs.getLED(1).setColor(LEDColor.CYAN);
        myLEDs.getLED(1).setOn();
        int speed = 1190;
        while (true)
        {
            directionServo.setValue(1500);
            speedServo.setValue(speed);
            Utils.sleep(1000);

            // for(int i=700;i<1700;i+=10){
            //     servo2.setValue(i);
            //     servo1.setValue(i+300);
            //     System.out.println("Speed:"+i);
            //     Utils.sleep(1000);
            //     servo2.setValue(0);
            //     Utils.sleep(10);
            // }
            Utils.sleep(10);
            speedServo.setValue(0);
            Utils.sleep(20000);
            
            System.out.println("Adjusting speed");
        }
    }
    
    /**
     * Turns the angle of the wheel in both directions for 10 seconds to calibrate
     * servo value to angle
     */
    private void runAngleCalibration()
    {
        myLEDs.getLED(2).setColor(LEDColor.CHARTREUSE);
        myLEDs.getLED(2).setOn();
        while (true)
        {
            directionServo.setValue(1000);
            Utils.sleep(10000);
            directionServo.setValue(2000);
            Utils.sleep(10000);
        }
    }

    private void check() throws MIDletStateChangeException {
        double frontright = 0;
        double frontleft = 0;
        double backright = 0;
        double backleft = 0;
        double rightdiff = 0;
        double leftdiff = 0;

        double frave = 0;
        double flave = 0;
        double brave = 0;
        double blave = 0;
        int dt = 250;

        try {
            for (int ii = 0; ii < dt; ii++) {
                frave += frSensor.getVoltage();
                flave += flSensor.getVoltage();
                brave += brSensor.getVoltage();
                blave += blSensor.getVoltage();
            }
//            frontright = sensor1.getVoltage();  
//            frontleft = sensor2.getVoltage(); 
//            backright = sensor3.getVoltage(); 
//            backleft = sensor4.getVoltage(); 

            frave /= dt;
            flave /= dt;
            brave /= dt;
            blave /= dt;

            rightdiff = frontright - backright;
            leftdiff = frontleft - backleft;

            //System.out.println("fr:" + frontright+"fl:" + frontleft+
            //"br:" + backright+"bl:" + backleft);
            System.out.println("frontright=" + frave);
            System.out.println("frontleft=" + flave);
            System.out.println("backright=" + brave);
            System.out.println("backleft=" + blave);

        } catch (IOException ex) {
            ex.printStackTrace();
        }


//        int dt = 500;
//
//        Utils.sleep(dt);

    }

    private double getCurrentAngle() throws MIDletStateChangeException {
        double frontright = 0;
        double frontleft = 0;
        double backright = 0;
        double backleft = 0;
        double fr = 0;
        double fl = 0;
        double br = 0;
        double bl = 0;

        int samples = 3;
        long time = 0;
        double diff=0;
        try {
            for (int ii = 0 ; ii < samples ; ii++)
            {
                frontright += frSensor.getVoltage();
                frontleft += flSensor.getVoltage();
                backright += brSensor.getVoltage();
                backleft += blSensor.getVoltage();
            }
            
            frontright /= (double) samples;
            frontleft /= (double) samples;
            backright /= (double) samples;
            backleft /= (double) samples;
            
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        
        /* Max out all sensors at 1 meter for best sensor behavior */
        if (frontleft <= 0.513)
        {
            frontleft = 0.513;
        }
        
        if (frontright <= 0.253)
        {
            frontright = 0.253;
        }
        
        if (backright <= 0.315)
        {
            backright = 0.315;
        }
        
        if (backleft <= 0.383)
        {
            backleft = 0.383;
        }

        fr = 1 / (frontright * 0.0175 + 0.000575);
        fl = 1/(frontleft * 0.0189 -0.0047);
        br = 1/(backright * 0.0168 -0.0003);
        bl = 1/(backleft * 0.0175 -0.0017);
        System.out.println("fr:"+fr+"fl:"+fl);
        if (fr < fl)
        {
            if (Math.abs(fr - br) < 35)
            {
                return fr-br;
            }
            else
            {
                return 0;
            }
        }
        else
        {
            if (Math.abs(fl-bl) < 35)
            {
                return bl-fl;
            }
            else
            {
                return 0;
            }
        }
//        if (fr - br < 0 )
//        {
//            if (fl - bl > 0)
//            {
//                return fr-br;
//            }
//            else
//            {
//                return 0;
//            }
//        }
//        else
//        {
//            if (fl - bl < 0)
//            {
//                return fr-br;
//            }
//            else
//            {
//                return 0;
//            }
//        }
//        if(fr+br<fl+bl){
////            if (Math.abs(fr-br) > 5)
////                System.out.println("fr:"+fr+"fl:"+fl+"br:"+br+"bl:"+bl);
//            return fr-br;
//        }
//        else{
////            if (Math.abs(bl-fl) > 5)
////                System.out.println("fr:"+fr+"fl:"+fl+"br:"+br+"bl:"+bl);
//            return bl-fl;
//        }
      
        
    }

    /**
     * Returns the right and left distances according to the front two sensors
     *
     * @return
     */
    private DistPair getCurrentDistance() 
    {
        double frontright = 0;
        double frontleft = 0;
        double backright = 0;
        double backleft = 0;
        double fr = 0;
        double fl = 0;
        double br = 0;
        double bl = 0;
        
        int samples = 3;
        long time = 0;
        double diff=0;
        try {
            for (int ii = 0 ; ii < samples ; ii++)
            {
                frontright += frSensor.getVoltage();
                frontleft += flSensor.getVoltage();
                backright += brSensor.getVoltage();
                backleft += blSensor.getVoltage();
            }
            
            frontright /= (double) samples;
            frontleft /= (double) samples;
            backright /= (double) samples;
            backleft /= (double) samples;
            
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        
        /* Max out all sensors at 2 meter for best sensor behavior */
        if (frontleft <= 0.513)
        {
            frontleft = 0.513;
        }
        
        if (frontright <= 0.253)
        {
            frontright = 0.253;
        }
        
        if (backright <= 0.315)
        {
            backright = 0.315;
        }
        
        if (backleft <= 0.383)
        {
            backleft = 0.383;
        }
        
//        System.out.println("frontright: " + frontright + " backright: " + backright);
        fr = 1 / (frontright * 0.0175 + 0.000575);
        fl = 1/(frontleft * 0.0189 -0.0047);
        br = 1/(backright * 0.0168 -0.0003);
        bl = 1/(backleft * 0.0175 -0.0017);
        
//        double rightAve = (fr + br) / 2;
//        double leftAve = (bl + fl) / 2;
//        
        /* Compensation for offset */
//        fl += 10;
        
//        double distDelta = Math.abs(rightAve - leftAve);
        
        System.out.println("front left distance = " + fl + " front Right distance = " + fr);
//        if (Math.abs(rightAve - leftAve) < 20)
//        {
//            return 0;
//        }

        return new DistPair(fl, fr);
    }

    private void setServoForwardValue() {
        servo1Left = current1 + step1;
        servo1Right = current1 - step1;
        servo2Forward = current2 + step2;
        servo2Back = current2 - step2;
        if (step2 == SERVO2_HIGH) {
            velocityBlinker.setColor(LEDColor.GREEN);
        } else {
            velocityBlinker.setColor(LEDColor.BLUE);
        }
    }

    private void adjustMovement(double diff, DistPair dist) throws MIDletStateChangeException 
    {
//        System.out.println("diff:"+diff+" distance:"+distanceOff);
        for (int i = 0; i < myLEDs.size(); i++) {
            myLEDs.getLED(i).setOff();
        }
        
        double leftDist = dist.getLeft();
        double rightDist = dist.getRight();
        if (Math.abs(diff) < maxAngle)
        {
            if (rightDist < wallDistance)
            {
                myLEDs.getLED(0).setColor(LEDColor.MAUVE);
                myLEDs.getLED(0).setOn();
                left(300);
            }
            else if (leftDist < wallDistance)
            {
                myLEDs.getLED(7).setColor(LEDColor.MAUVE);
                myLEDs.getLED(7).setOn();
                right(300);
            }
            else
            {
                myLEDs.getLED(3).setColor(LEDColor.GREEN);
                myLEDs.getLED(4).setColor(LEDColor.GREEN);
                myLEDs.getLED(4).setOn();
                myLEDs.getLED(3).setOn();
                straight(500);
            }
        }
        else if(diff > maxAngle && rightDist < wallDistance )
       {
                
           myLEDs.getLED(0).setColor(LEDColor.YELLOW);
           myLEDs.getLED(0).setOn();
               left(300);
       }
       else if (diff > maxAngle && leftDist < wallDistance)
       {
           myLEDs.getLED(7).setColor(LEDColor.YELLOW);
           myLEDs.getLED(7).setOn();
               right(300);

       }
       else if (diff < -maxAngle && rightDist < wallDistance)
       {
           myLEDs.getLED(0).setColor(LEDColor.RED);
           myLEDs.getLED(0).setOn();
               left(300);
       }
       else if (diff < -maxAngle && leftDist < wallDistance) 
       {
           myLEDs.getLED(7).setColor(LEDColor.RED);
           myLEDs.getLED(7).setOn();
               right(300);
       }
       else if (diff > maxAngle)
       {
           myLEDs.getLED(7).setColor(LEDColor.BLUE);
           myLEDs.getLED(7).setOn();
           right(300);
       }
       else if (diff < -maxAngle)
       {
           myLEDs.getLED(0).setColor(LEDColor.BLUE);
           myLEDs.getLED(0).setOn();
           left(300);
       }
       else
       {
           myLEDs.getLED(3).setColor(LEDColor.GREEN);
           myLEDs.getLED(4).setColor(LEDColor.GREEN);
           myLEDs.getLED(4).setOn();
           myLEDs.getLED(3).setOn();
           straight(200);
       }
    }

    /**
     * Adjusts the distance of the car so that it is equidistant from the left
     * and right walls
     *
     * @param velocity
     * @param distance
     */
    private void adjustDistance(double velocity, double distance) {
    }

    /**
     * Starts the car moving forward very slowly
     */
    private void beginForwardMotion() 
    {
        myLEDs.getLED(1).setColor(LEDColor.MAUVE);
        myLEDs.getLED(1).setOn();
        
        directionServo.setValue(1500);
        speedServo.setValue(speed);
    }

    private void right(long time) {
//        servo1.setValue(speed);
        directionServo.setValue(1150);
        Utils.sleep(time);
//        servo1.setValue(0);
        //servo1.setValue(0);
        directionServo.setValue(1500);
    }
    
    
    

    private void left(long time) {
//        servo1.setValue(speed);
        directionServo.setValue(1700);
        Utils.sleep(time);
//        servo1.setValue(0);
        directionServo.setValue(1500);
    }
    
    private void straight (long time) {
//        servo1.setValue(speed);
        directionServo.setValue(1500);
        Utils.sleep(time);
//        servo1.setValue(0);
    }

    private void stop() {
        //System.out.println("stop");
        //servo1.setValue(0);
        directionServo.setValue(SERVO_CENTER_VALUE);
    }

//    private void backward() {
//        System.out.println("forward");
//        //servo1.setValue(0);
////        current2= servo2.getValue();
////
////        for (int i=0;i<3;i++){
////            current2= servo2.getValue();
////            if (current2 + step2 <SERVO2_MAX_VALUE){
////                servo2.setValue(current2+step2);
////                Utils.sleep(50);
////            } else {
////                servo2.setValue(SERVO2_MAX_VALUE);
////                Utils.sleep(50);
////            }
////        }
//        servo1.setValue(0);
//        servo2.setValue(1600);
//        Utils.sleep(30);
//        servo2.setValue(0);
//
//        /*     while(current2 + step2 <SERVO2_MAX_VALUE){
//         servo2.setValue(current2+step2);
//         current2= servo2.getValue();
//         Utils.sleep(50);
//         }*/
//    }
//
//    private void forward() {
//        System.out.println("backward");
//        //servo1.setValue(0);
////        current2 = servo2.getValue();
////
////        for (int i=0;i<3;i++){
////            current2 = servo2.getValue();
////            if (current2 -step2>SERVO2_MIN_VALUE){
////                servo2.setValue(current2-step2);
////                Utils.sleep(50);
////            } else {
////                servo2.setValue(SERVO2_MIN_VALUE);
////                Utils.sleep(50);
////            }
////        } 
//
////        servo1.setValue(0);
////        servo2.setValue(1260);
////        Utils.sleep(30);
////        servo2.setValue(0);
////        
//        servo1.setValue(0);
//        Utils.sleep(1000);
//        for (int i = 700; i < 1700; i += 10) {
//            servo2.setValue(i);
//            servo1.setValue(i + 300);
//            System.out.println("Speed:" + i);
//            Utils.sleep(1000);
//            servo2.setValue(0);
//            Utils.sleep(10);
//        }
//        /*    while (current2 - step2 > SERVO2_MIN_VALUE){
//         servo2.setValue(current2-step2);
//         current2 = servo2.getValue();
//         Utils.sleep(50);
//         }*/
//    }

    /*  private void leftRotation() {
     System.out.println("leftRotation");
     servo1.setValue(servo1Left);
     servo2.setValue(servo2Forward);
     }*/

    /*   private void rightRotation() {
     System.out.println("rightRotation");
     servo1.setValue(servo1Right);
     servo2.setValue(servo2Back);
     }*/
    public void switchPressed(SwitchEvent sw) {
        System.out.println("SW Pressed" + sw);
        if (sw.getSwitch() == sw1) { // for program change
            if (++program > PROG1) {
                program = PROG0;
            }
            switch (program) {
                case PROG0:
                    progBlinker.setColor(LEDColor.BLUE);
                    break;
                case PROG1:
                    progBlinker.setColor(LEDColor.GREEN);
                    break;
                /*                case PROG2:
                 progBlinker.setColor(LEDColor.YELLOW);
                 break;*/
            }
        } else if (sw.getSwitch() == sw2) { // for velocity change
            step1 = (step1 == SERVO1_HIGH) ? SERVO1_LOW : SERVO1_HIGH;
            step2 = (step2 == SERVO2_HIGH) ? SERVO2_LOW : SERVO2_HIGH;
            setServoForwardValue();
        }
    }

    public void switchReleased(SwitchEvent sw) {
        // do nothing
    }

    protected void pauseApp() {
        // This will never be called by the Squawk VM
    }

    /**
     * Called if the MIDlet is terminated by the system. I.e. if startApp throws
     * any exception other than MIDletStateChangeException, if the isolate
     * running the MIDlet is killed with Isolate.exit(), or if VM.stopVM() is
     * called.
     *
     * It is not called if MIDlet.notifyDestroyed() was called.
     *
     * @param unconditional If true when this method is called, the MIDlet must
     * cleanup and release all resources. If false the MIDlet may throw
     * MIDletStateChangeException to indicate it does not want to be destroyed
     * at this time.
     */
    protected void destroyApp(boolean unconditional) throws MIDletStateChangeException {
        for (int i = 0; i < myLEDs.size(); i++) {
            myLEDs.getLED(i).setOff();
        }
    }
}
