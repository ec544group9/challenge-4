/*
 * Copyright (c) 2006-2010 Sun Microsystems, Inc.
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to 
 * deal in the Software without restriction, including without limitation the 
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or 
 * sell copies of the Software, and to permit persons to whom the Software is 
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in 
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE 
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 * DEALINGS IN THE SOFTWARE.
 **/

package org.sunspotworld;

import com.sun.spot.resources.Resources;
import com.sun.spot.resources.transducers.ISwitch;
import com.sun.spot.resources.transducers.ISwitchListener;
import com.sun.spot.resources.transducers.ITriColorLED;
import com.sun.spot.resources.transducers.ITriColorLEDArray;
import com.sun.spot.resources.transducers.LEDColor;
import com.sun.spot.resources.transducers.SwitchEvent;
import com.sun.spot.sensorboard.EDemoBoard;
import com.sun.spot.sensorboard.peripheral.Servo;
import com.sun.spot.service.BootloaderListenerService;
import com.sun.spot.util.Utils;
import javax.microedition.midlet.MIDlet;
import javax.microedition.midlet.MIDletStateChangeException;
import org.sunspotworld.common.Globals;
import org.sunspotworld.common.TwoSidedArray;
import org.sunspotworld.lib.BlinkenLights;


import com.sun.spot.peripheral.radio.RadioFactory;
import com.sun.spot.resources.Resources;
//import com.sun.spot.resources.transducers.IIOPin;
import com.sun.spot.resources.transducers.IAnalogInput;
//import com.sun.spot.sensorboard.io.AnalogInput;
import com.sun.spot.resources.transducers.ISwitch;
import com.sun.spot.resources.transducers.ITriColorLED;
import com.sun.spot.resources.transducers.ITriColorLEDArray;
import com.sun.spot.sensorboard.EDemoBoard;
import com.sun.spot.service.BootloaderListenerService;
import com.sun.spot.util.IEEEAddress;
import com.sun.spot.util.Utils;

import java.io.IOException;
import javax.microedition.midlet.MIDlet;
import javax.microedition.midlet.MIDletStateChangeException;
/**
 * This class is used to move a servo car consisting of two servos - one for
 * left wheel and the other for right wheel. To combine these servos properly,
 * this servo car moves forward/backward, turn right/left and rotate
 * clockwise/counterclockwise.
 * 
 * The current implementation has 3 modes and you can change these "moving mode" 
 * by pressing sw1. Mode 1 is "Normal" mode moving the car according to the tilt 
 * of the remote controller. Mode 2 is "Reverse" mode moving the car in 
 * a direction opposite to Mode 1. Mode 3 is "Rotation" mode only rotating the
 * car clockwise or counterclockwise according to the tilt.
 * 
 * @author Tsuyoshi Miyake <Tsuyoshi.Miyake@Sun.COM>
 * @author Yuting Zhang<ytzhang@bu.edu>
 */
public class ServoSPOTonCar extends MIDlet implements ISwitchListener {
    private static final int SERVO_CENTER_VALUE = 1500;
    //private static final int SERVO_MAX_VALUE = 2000;
    //private static final int SERVO_MIN_VALUE = 1000;
    private static final int SERVO1_MAX_VALUE = 2000;
    private static final int SERVO1_MIN_VALUE = 1000;
    private static final int SERVO2_MAX_VALUE = 2000;
    private static final int SERVO2_MIN_VALUE = 1000;
    //private static final int SERVO_HIGH = 500;
    //private static final int SERVO_LOW = 300;
    private static final int SERVO1_HIGH = 20; //steering step high
    private static final int SERVO1_LOW = 10; //steering step low
    private static final int SERVO2_HIGH = 10; //speeding step high
    private static final int SERVO2_LOW = 5; //speeding step low
    private static final int PROG0 = 0; //default program
    private static final int PROG1 = 1; // reversed program
    //private static final int PROG2 = 2;
    // Devices
    private EDemoBoard eDemo = EDemoBoard.getInstance();
    private ISwitch sw1 = eDemo.getSwitches()[EDemoBoard.SW1];
    private ISwitch sw2 = eDemo.getSwitches()[EDemoBoard.SW2];
    private ITriColorLED[] leds = eDemo.getLEDs();
    private ITriColorLEDArray myLEDs = (ITriColorLEDArray) Resources.lookup(ITriColorLEDArray.class);
    
    // 1st servo for left & right direction 
    private Servo speedServo = new Servo(eDemo.getOutputPins()[EDemoBoard.H1]);
    // 2nd servo for forward & backward direction
    private Servo directionServo = new Servo(eDemo.getOutputPins()[EDemoBoard.H0]);
    private BlinkenLights progBlinker = new BlinkenLights(0, 3);
    private BlinkenLights velocityBlinker = new BlinkenLights(4, 7);
    private int current1 = SERVO_CENTER_VALUE;
    private int current2 = SERVO_CENTER_VALUE;
    private int step1 = SERVO1_LOW;
    private int step2 = SERVO2_LOW;
    private int program = PROG0;
    
    IAnalogInput sensor1 = EDemoBoard.getInstance().getAnalogInputs()[EDemoBoard.A0];//frontright
    IAnalogInput sensor2 = EDemoBoard.getInstance().getAnalogInputs()[EDemoBoard.A1];//frontleft
    IAnalogInput sensor3 = EDemoBoard.getInstance().getAnalogInputs()[EDemoBoard.A2];//backright
    IAnalogInput sensor4 = EDemoBoard.getInstance().getAnalogInputs()[EDemoBoard.A3];//backleft
    
    
    //private int servo1ForwardValue;
    //private int servo2ForwardValue;
    private int servo1Left = SERVO_CENTER_VALUE +SERVO1_LOW;
    private int servo1Right = SERVO_CENTER_VALUE -SERVO1_LOW;
    private int servo2Forward = SERVO_CENTER_VALUE +SERVO2_LOW;
    private int servo2Back = SERVO_CENTER_VALUE - SERVO2_LOW;
    
    public ServoSPOTonCar() {
    }
    
    /** BASIC STARTUP CODE **/
    protected void startApp() throws MIDletStateChangeException {
        System.out.println("Hello, world");
        BootloaderListenerService.getInstance().start();  

        for (int i = 0; i < myLEDs.size(); i++) {
            myLEDs.getLED(i).setColor(LEDColor.GREEN);
            myLEDs.getLED(i).setOn();
        }
        Utils.sleep(500);
        for (int i = 0; i < myLEDs.size(); i++) {
            myLEDs.getLED(i).setOff(); 
        }
        setServoForwardValue();
        progBlinker.startPsilon();
        velocityBlinker.startPsilon();
        // timeout 1000
//        TwoSidedArray robot = new TwoSidedArray(getAppProperty("buddyAddress"), Globals.READ_TIMEOUT);
//        try {
//            robot.startInput();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//        sw1.addISwitchListener(this);
//        sw2.addISwitchListener(this);
//
//        velocityBlinker.setColor(LEDColor.BLUE);
//        progBlinker.setColor(LEDColor.BLUE);
//
//        boolean error = false;
        while (sw1.isOpen());
        forward();
        
        /* Assume the car is initially straight and in the middle of the hallway */
        double carAngle = 0;
        double distanceOff = 0;
//        beginForwardMotion();
        
        while (true) {
            /* This is the control loop for the car, multiple functions are called from here */
//            carAngle = getCurrentAngle();
//            adjustAngle(10.0, carAngle);
//            
//            /* If angle or distance is sufficiently off, can force the car to stop to fix it */
//            
//            distanceOff = getCurrentDistance();
//            adjustDistance(10.0, distanceOff);
            
            /* This repeats to continue editing the car position, the car is always
             * moving slowly forward 
             */
            
            
//            boolean timeoutError = robot.isTimeoutError();
//            int xtilt = 0;
//            int ytilt = 0;
//
//            if (!timeoutError) {
//                xtilt = robot.getVal(0);
//                ytilt = robot.getVal(1);
//                if (error) {
//                    step1 = SERVO1_LOW;
//                    step2 = SERVO2_LOW;
//                    program = PROG0;
//                    velocityBlinker.setColor(LEDColor.BLUE);
//                    progBlinker.setColor(LEDColor.BLUE);
//                    error = false;
//                }
//            } else {
//                velocityBlinker.setColor(LEDColor.RED);
//                progBlinker.setColor(LEDColor.RED);
//                error = true;
//            }
            /* Control System here */
            
            
//            switch (program) {
//                case PROG0:
//                    program0(xtilt, ytilt);
//                    break;
//                case PROG1:
//                    program1(xtilt, ytilt);
//                    break;
// //             case PROG2:
// //                 program2(xtilt, ytilt);
// //                 break;
//            }
            
            //pidControl(1);
//            Utils.sleep(20);
        }
    }
    
    /**
     * Returns the current angle of the car based on the sensors
     * @return 
     */
    private double getCurrentAngle()
    {
        return 0;
    }
    
    /**
     * Gets the current distance differential from center 
     * 
     * A positive value is to the right of center, a negative is to 
     * the left
     * @return 
     */
    private double getCurrentDistance()
    {
      
        return 0;
    }
    
    /**
     * Adjusts the angle of the car so that is is parallel with the wall
     * @param velocity - the velocity of the car
     * @param angle - the current angle of the car
     */
    private void adjustAngle(double velocity, double angle)
    {
        
    }
    
    /**
     * Adjusts the distance of the car so that it is equidistant from the left
     * and right walls
     * @param velocity
     * @param distance 
     */
    private void adjustDistance(double velocity, double distance)
    {
        
    }
    
    /**
     * Starts the car moving forward very slowly
     */
    private void beginForwardMotion()
    {
        
    }
    
    private void pidControl(double setpoint) throws MIDletStateChangeException{
      
        
        double frontright = 0;
        double frontleft = 0;
        double backright = 0;
        double backleft = 0;
        try{
            frontright = sensor1.getVoltage();  
            frontleft = sensor2.getVoltage(); 
            backright = sensor3.getVoltage(); 
            backleft = sensor4.getVoltage(); 
            System.out.println("fr:" + frontright+"fl:" + frontleft+
                    "br:" + backright+"bl:" + backleft);
        }
        catch (IOException ex){
                ex.printStackTrace();
        }
        

        int dt = 1000;
//        double previous_error = setpoint - before;
//        double integral = 0;
//        
//        double Kp=1;
//        double Ki=1;
//        double Kd=1;
//        
        /* Sleep to get next reading */
        Utils.sleep(dt);
        
//        double after = 0;
//        try{
//            after = sensor.getVoltage();  
////            System.out.println("Value: " + after);
//        }
//        catch (IOException ex){
//                ex.printStackTrace();
//        }
//        double error = setpoint - after;
//        System.out.println(setpoint + " - " + after + " = " + error);
//        
//        integral = integral + (error * dt);
//        
//        double derivative = (error - previous_error) / dt;
//        
//        double output = (Kp * error) + (Ki * integral) + (Kd * derivative);
//        
////        System.out.println("error: " + error);
//        if (Math.abs(error) > 0.1f)
//        {    
//            if (error > 0)
//                forward();
//            else
//                backward();
//        }

    }
    
    
        
    private void setServoForwardValue(){
        servo1Left = current1 + step1;
        servo1Right = current1 - step1;
        servo2Forward = current2 + step2;
        servo2Back = current2 - step2;
        if (step2 == SERVO2_HIGH) {
            velocityBlinker.setColor(LEDColor.GREEN);
        } else {
            velocityBlinker.setColor(LEDColor.BLUE);
        }
    }

    // Normal Mode
    private void program0(int xtilt, int ytilt) {
        if (ytilt > 40) {
            forward();
        } else if (ytilt < -40) {
            backward();
        } else if (xtilt > 40) {
            right();
        } else if (xtilt < -40) {
            left();
        } else {
            stop();
        }
    }

    // Reverse Mode
    private void program1(int xtilt, int ytilt) {
        if (ytilt > 40) {
            backward();
        } else if (ytilt < -40) {
            forward();
        } else if (xtilt > 40) {
            left();
        } else if (xtilt < -40) {
            right();
        } else {
            stop();
        }
    }


    private void left() {
        System.out.println("left");
        current1 = speedServo.getValue();
        if (current1 + step1 < SERVO1_MAX_VALUE){
            speedServo.setValue(current1+step1);
            Utils.sleep(50);
        } else {
            speedServo.setValue(SERVO1_MAX_VALUE);
            Utils.sleep(50);
        }
    }

    private void right() {
        System.out.println("right");
        current1 = speedServo.getValue();
        if (current1-step1 > SERVO1_MIN_VALUE){
            speedServo.setValue(current1-step1);
            Utils.sleep(50);
        } else {
            speedServo.setValue(SERVO1_MIN_VALUE);
            Utils.sleep(50);
        }
        //servo2.setValue(0);
    }

    private void stop() {
        //System.out.println("stop");
        //servo1.setValue(0);
        directionServo.setValue(SERVO_CENTER_VALUE);
    }

    private void backward() {
        System.out.println("forward");
        //servo1.setValue(0);
//        current2= directionServo.getValue();
//
//        for (int i=0;i<3;i++){
//            current2= directionServo.getValue();
//            if (current2 + step2 <SERVO2_MAX_VALUE){
//                directionServo.setValue(current2+step2);
//                Utils.sleep(50);
//            } else {
//                directionServo.setValue(SERVO2_MAX_VALUE);
//                Utils.sleep(50);
//            }
//        }
        speedServo.setValue(0);
        directionServo.setValue(1600);
        Utils.sleep(30);
        directionServo.setValue(0);
            
  /*     while(current2 + step2 <SERVO2_MAX_VALUE){
            directionServo.setValue(current2+step2);
            current2= directionServo.getValue();
            Utils.sleep(50);
         }*/
    }

    private void forward() {
        System.out.println("backward");
        //servo1.setValue(0);
//        current2 = directionServo.getValue();
//
//        for (int i=0;i<3;i++){
//            current2 = directionServo.getValue();
//            if (current2 -step2>SERVO2_MIN_VALUE){
//                directionServo.setValue(current2-step2);
//                Utils.sleep(50);
//            } else {
//                directionServo.setValue(SERVO2_MIN_VALUE);
//                Utils.sleep(50);
//            }
//        } 
        
//        speedServo.setValue(0);
//        directionServo.setValue(1260);
//        Utils.sleep(30);
//        directionServo.setValue(0);
//        
        speedServo.setValue(0);
        directionServo.setValue(1500);
        Utils.sleep(1000);
        for(int i=1000;i<2000;i+=10){
            speedServo.setValue(i);
            directionServo.setValue(i);
            System.out.println("Speed:"+i);
            Utils.sleep(1000);
            Utils.sleep(10);
        }
    /*    while (current2 - step2 > SERVO2_MIN_VALUE){
        directionServo.setValue(current2-step2);
        current2 = directionServo.getValue();
        Utils.sleep(50);
                }*/
    }

  /*  private void leftRotation() {
        System.out.println("leftRotation");
        speedServo.setValue(servo1Left);
        directionServo.setValue(servo2Forward);
    }*/

 /*   private void rightRotation() {
        System.out.println("rightRotation");
        speedServo.setValue(servo1Right);
        directionServo.setValue(servo2Back);
    }*/

    public void switchPressed(SwitchEvent sw) {
        System.out.println("SW Pressed" + sw);
        if (sw.getSwitch() == sw1) { // for program change
            if (++program > PROG1) {
                program = PROG0;
            }
            switch (program) {
                case PROG0:
                    progBlinker.setColor(LEDColor.BLUE);
                    break;
                case PROG1:
                    progBlinker.setColor(LEDColor.GREEN);
                    break;
/*                case PROG2:
                    progBlinker.setColor(LEDColor.YELLOW);
                    break;*/
            }
        } else if (sw.getSwitch() == sw2) { // for velocity change
            step1 = (step1 == SERVO1_HIGH) ? SERVO1_LOW : SERVO1_HIGH;
            step2 = (step2 == SERVO2_HIGH) ? SERVO2_LOW : SERVO2_HIGH;
            setServoForwardValue();
        }
    }

    public void switchReleased(SwitchEvent sw) {
    // do nothing
    }
    
    protected void pauseApp() {
        // This will never be called by the Squawk VM
    }
    
    /**
     * Called if the MIDlet is terminated by the system.
     * I.e. if startApp throws any exception other than MIDletStateChangeException,
     * if the isolate running the MIDlet is killed with Isolate.exit(), or
     * if VM.stopVM() is called.
     * 
     * It is not called if MIDlet.notifyDestroyed() was called.
     *
     * @param unconditional If true when this method is called, the MIDlet must
     *    cleanup and release all resources. If false the MIDlet may throw
     *    MIDletStateChangeException  to indicate it does not want to be destroyed
     *    at this time.
     */
    protected void destroyApp(boolean unconditional) throws MIDletStateChangeException {
        for (int i = 0; i < myLEDs.size(); i++) {
            myLEDs.getLED(i).setOff();
        }
    }

}

